---
layout: post
title: "Responsive CSS"
categories: CSS
---

When making a site, the @media tag can be used to make more responsive css.

<pre>@media only screen and (max-width: 720px) {
    .title {
        font-size:3rem;
    }
    .subtitle {
        font-size: 1.5rem;
        padding:3%;
    }
}</pre>

`max-width` can be replaced with `min-width` for the opposite result
---
layout: post
title: Styling Images
categories: CSS
---

Here are a few options for manipulating images with CSS.
Go knock yourself out:

- `border-radius` can be used to make the edges of the images curved.

- `padding` creates a border around the image. Can be used for thumbnails.

- `box-shadow` gives the element a shadow. Here are more values <a href="https://www.w3schools.com/cssref/css3_pr_box-shadow.asp">at w3schools</a>



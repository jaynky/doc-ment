---
layout: post
title: If statements
categories: Jekyll
---

If statements in liquid can be used to only load certain sections based on what you define in the front matter. For example, if you only want a JS library loaded on a certain page, you could employ a liquid boolean.


<pre>
{% raw %}
(in front matter)

comments: true

{% if page.comments %}

Stuff

{% endif %}


{% endraw %}
</pre>
---
layout: post
title: Cicular Progress Bar
categories: CSS
---

Cicular radial dial Progress bars are LIT! They make things look cool.

You will need a circle.css library to start.

In the library you can change around colors and values.
Clunky I know.

Until my future self finds a better way, this is the way to build it.

```
 <div class="card clearfix dark-area col-md-4">

                <div class="c100 p78 small blue">
                    <span>78%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h3 class="grey">CSS</h3>
                </div>
```


Use the `pX` selector to name the percentage of the bar.


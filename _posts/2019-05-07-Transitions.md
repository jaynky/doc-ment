---
layout: post
title: Transitions
categories: CSS
---

Transitions are pretty great. They are fast ways to make a site look more elegant and professional.

Here is a code sample:
<pre>
.orange-btn:hover {
    color:black;
    background-color: orange;
    transition: 0.3s;
}
</pre>

Its rather self-explanatory
`trasnition: 0.3s;` is how long the animation will last. I also think it only really works on `:hover`
---
layout: post
title: "CSS Box-Sizing"
categories: CSS
---

Here is a useful image when determing how css elements
affect box sizing.

<img src="/images/box-model.png" class="col-md-12">
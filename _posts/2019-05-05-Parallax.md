---
layout: post
title:  "Parallax"
categories: CSS
---

There are two ways you can do parallax scrolling

### Method 1 (recommended):

Use 
<pre>
selector {
    background-image: url(src);
    background-size: cover;
}
</pre>

### Method 2:

In HTMl


`<img src="image/example" class="fixed">`


In CSS

<pre>
.fixed {
    z-index: -1
    position:fixed;
}
</pre>

This second option is less preferrable as you will only be able 
to use it on one image, or have multiple z-index values.

---
layout: post
title:  "Jekyll Posts"
categories: Jekyll
---

Using liquid to write posts.

There are multiple ways you can list posts on a homepage or blog.

{% raw %}


     {% for post in site.posts %}

        <!--Post-->
 
        <div class="post">
 
          <h1 class="post-title text-center">
 
              <div class="nline"><a href=" {{ post.url }}">{{ post.title }}</a></div>
 
          </h1>
 
          <!--Date-->
 
            <p class="small post-date text-center">{{ post.date | date_to_string }}</p>

            <p>{{ post.content }}</p>
 
            <div class="d-flex p-4 bg-transparent"></div>
 
        {% endfor %}


{% endraw %}



{% raw %}

`{{post.content}}` displays the entire post.

`{{post.excerpt}}` displays the post's expert (as defined in front matter)
 
`strip_html` removes any and all html elements including pictures that use `<img>` tags

`{{ page.author }}` author as defined in front matter

`{{ post.date }}` date 

`date_to_string` translates the date into a readable string

{% endraw %}
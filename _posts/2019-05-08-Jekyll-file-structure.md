---
layout: post
title: Jekyll file structure (updated 5/8/2019)
categories: Jekyll
---

Over time I have developed a optimal structure tree for Jekyll.
There a few things to point out first.

-There is no Sass files. This is because for optimal performance all the CSS is placed in an includes, and we have not worked out a way to pipe the compiled sass to a CSS includes file.

-Markdown files are only used for front matter. Having modular layouts means its easier to swap and experiment. Also VS Code does not have similar auto complete for front matter as Markdown. 

-Instead of an assets folder there is an `/images` making it much easier to reach

-All css is in `_includes`



<pre>
.
├── _data
├── images
│   └── icons
├── _includes
├── _layouts
├── _posts
│   └── _site
└── _site
    ├── assets
    ├── images
    │   └── icons
    └── jekyll
        └── update
            └── 2019
                └── 04
                    └── 16

</pre>

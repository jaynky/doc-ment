---
layout: post
title:  "Autogen Basic Jekyll Files"
categories: Pipeline
---

Coding the same includes and doing the same adjustments everytime I made a new site got boring and a major waste of time. To fix this situation, I wrote a simple script that copies files from the `AugustusFiles` directory to the jekyll folder that you specify.

Here is the code 

```
#!/bin/bash

echo "filename"

read filename

mkdir $filename/_includes
mkdir $filename/_layouts
cp AugustusFiles/head.html $filename/_includes/
cp AugustusFiles/animate.css $filename/_includes/
cp AugustusFiles/bootstrap.css $filename/_includes/
cp AugustusFiles/footer.html $filename/_includes/
cp AugustusFiles/Jstyle.css $filename/_includes/
cp AugustusFiles/_config.yml $filename/
rm $filename/about.md

```

Its pretty straightforward. You write the files in `/AugustusFiles` that you want with every site you make, run `./Augustus.sh` then enter the filename of your jekyll file.
---
layout: post
title:  "display:none"
categories: CSS
---

When trying to hide elements such as when you are 
using the `@media` tag to create a responsive website
`display:none` can be used to remove an element entirely.

<pre>
@media only screen and (max-width: 900px) {

    .description {
        display: none;
    }
}
</pre>


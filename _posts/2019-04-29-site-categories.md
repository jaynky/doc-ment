---
layout: post
title:  "Site Categories"
categories: Jekyll
---

Site categories are declared in the front matter
and can be used in different ways to categorize posts.


#### Archive
Categories can be used to list posts as follows:


{%raw%}


{% for category in site.categories %}
  
  `<h3>` {{ category[0] }}`</h3>`

  `<ul>`

    {% for post in category[1] %}
  
      <li><a href="{{ post.url }}">{{ post.title }}
      
      </a></li>
    
   {% endfor %}
  
  </ul>

{% endfor %}


{%endraw%}

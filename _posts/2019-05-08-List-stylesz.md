---
layout: post
title: List Stylez
categories: CSS
---

Styling lists can really make a site pop.
All you have to do is use the `list-style-type` property and pair it with the value you want.
<a href="https://www.w3schools.com/cssref/pr_list-style-type.asp">W3schools has a nice list of all the values.</a>

Here is some example code:

<pre>
li {
    list-style-type: circle;
}
</pre>